/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.labox;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class LabOX {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char player = 'X';
    static int row, col;
    static Scanner kb = new Scanner(System.in);

    static void printWelcome() {
        System.out.println("Welcome to XO");
    }

    static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    static void printTurn() {
        System.out.println(player + " Turn");
    }

    static void inputRowCol() {
        
        while (true) {
            System.out.print("Please input row,col : ");
            row = kb.nextInt();
            col = kb.nextInt();
            if (table[row - 1][col - 1] == '-') {
                table[row - 1][col - 1] = player;
                break;
            }

        }
    }

    static void changePlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static boolean isWin() {
        if (checkRowWin()) {
            return true;
        }
        if (checkColWin()) {
            return true;
        }
        if (checkDiagonalWin()) {
            return true;
        }
        return false;
    }

    static boolean isDraw() {
        if (checkDraw()) {
            return true;
        }
        return false;
    }

    static void printWin() {
        System.out.println(player + " is a winner!");
    }

    static void printDraw() {
        System.out.println("Tie No one win");
    }

    static boolean checkRowWin() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkColWin() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != player) {
                return false;
            }
        }
        return true;
    }

    static boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    static boolean checkDiagonalWin() {
        if (table[0][0] == player && table[1][1] == player && table[2][2] == player) {
            return true;
        }
        if (table[2][0] == player && table[1][1] == player && table[0][2] == player) {
            return true;
        }
        return false;
    }

    static void resetTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                table[i][j] = '-';
            }
        }
    }

    static void resetTurn() {
        player = 'X';
    }

    public static void main(String[] args) {
        printWelcome();
        while(true){
            resetTable();
            resetTurn();
            while (true) {
                printTable();
                printTurn();
                inputRowCol();
                if (isWin()) {
                    printTable();
                    printWin();
                    break;
                } else if (isDraw()) {
                    printTable();
                    printDraw();
                    break;

                }
                changePlayer();

            }
            System.out.print("Please input continue or exit: ");
            String con = kb.next();
            if (con.equals("continue")){
                continue;
            }else if(con.equals("exit")){
                break;
            }
        }

    }
}
